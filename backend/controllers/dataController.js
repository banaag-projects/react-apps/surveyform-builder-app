const Data = require("../models/dataSchema");

module.exports.addData = (req, res) => {
  console.log(req.body);

  const newData = new Data({
    questions: req.body.questions,
    title: req.body.title,
  });

  newData
    .save()
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};
