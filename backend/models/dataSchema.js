const mongoose = require("mongoose");

const optionsSchema = new mongoose.Schema({
  optionText: String,
  selected: Boolean,
});

const questionSchema = new mongoose.Schema({
  questionText: String,
  questionType: String,
  options: [optionsSchema],
});

const dataSchema = new mongoose.Schema({
  title: { docTitle: String, docDesc: String },
  questions: [questionSchema],
});

// if may [] yung questionSchema lumalabas yung questions: Array(0)

const Data = mongoose.model("survey-response", dataSchema);
module.exports = Data;
