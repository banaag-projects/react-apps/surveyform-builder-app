const express = require("express");
const router = express.Router();
const { addData } = require("../controllers/dataController");

router.post("/add", addData);

module.exports = router;
