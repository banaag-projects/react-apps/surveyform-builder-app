const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

const app = express();
const port = 5000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
  "mongodb+srv://admin:admin@surveyformsdata.pwuhg8v.mongodb.net/surveydata?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to the cloud database"));

const dataRouter = require("./routes/dataRoute");
app.use("/data", dataRouter);

app.listen(port, () => console.log(`Server is running at port ${port}`));
