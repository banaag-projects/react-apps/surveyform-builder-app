import React from "react";
import Header from "./components/Header";
import QuestionForm from "./components/QuestionForm";

function App() {
  return (
    <>
      <Header />
      <QuestionForm />
    </>
  );
}

export default App;
