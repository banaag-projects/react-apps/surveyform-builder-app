import React from "react";
import "./Header.css";

function Header() {
  return (
    <div className="header">
      <div className="header_info">
        <h2>SURVEY FORM BUILDER</h2>
      </div>
    </div>
  );
}

export default Header;
