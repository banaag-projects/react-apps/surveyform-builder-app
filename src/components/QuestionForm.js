import React, { useRef, useState } from "react";
import "./QuestionForm.css";
import axios from "axios";
import { FormControl, TextField } from "@mui/material";
import { RadioGroup } from "@mui/material";
import { FormControlLabel } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { Radio } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import PlaylistAddIcon from "@mui/icons-material/PlaylistAdd";
import { Grid } from "@mui/material";

function QuestionForm() {
  var docTitleRef = useRef();
  var docDescRef = useRef();

  const [questions, setQuestions] = useState([
    {
      questionText: "",
      questionType: "Radio",
      options: [
        { optionText: "Option 1", selected: false },
        { optionText: "Option 2", selected: false },
        { optionText: "Option 3", selected: false },
        { optionText: "Option 4", selected: false },
      ],
    },
  ]);

  function addQuestionField() {
    var addQuestions = [...questions];
    addQuestions.push({
      questionText: "",
      questionType: "Radio",
      options: [
        { optionText: "Option 1", selected: false },
        { optionText: "Option 2", selected: false },
        { optionText: "Option 3", selected: false },
        { optionText: "Option 4", selected: false },
      ],
    });
    setQuestions(addQuestions);
  }

  function changeQuestion(text, i) {
    setQuestions((prevState) => {
      const newQuestions = [...prevState];
      newQuestions[i].questionText = text;
      return newQuestions;
    });
  }
  function addOption(i) {
    var newOptions = [...questions];

    if (newOptions[i].options.length < 4) {
      newOptions[i].options.push({
        optionText: "Option",
      });
    } else {
      console.log("Max of 4 options per question only");
    }

    setQuestions(newOptions);
  }

  function removeQuestionField(i) {
    setQuestions((prevState) => {
      const newQuestions = [...prevState];
      newQuestions.splice(i, 1);
      return newQuestions;
    });
  }

  function changeOptionValue(text, i, j) {
    setQuestions((prevState) => {
      const optionsQuestion = [...prevState];
      optionsQuestion[i].options[j].optionText = text;
      return optionsQuestion;
    });
  }

  function handleRadioChange(index, i, j) {
    setQuestions((prevState) => {
      const radioQuestions = [...prevState];
      radioQuestions[i].options.forEach((option, index) => {
        if (j === index) {
          option.selected = true;
        } else {
          option.selected = false;
        }
      });
      return radioQuestions;
    });
  }
  function removeOption(i, j) {
    setQuestions((prevState) => {
      const removeOptionQuestion = [...prevState];
      if (removeOptionQuestion[i].options.length > 1) {
        removeOptionQuestion[i].options.splice(j, 1);
        return removeOptionQuestion;
      } else {
        return removeOptionQuestion;
      }
    });
  }

  function commitToDB(e) {
    e.preventDefault();

    const title = {
      docTitle: docTitleRef.current.value,
      docDesc: docDescRef.current.value,
    };
    console.log(title);

    axios
      .post("http://localhost:5000/data/add", {
        title,
        questions,
      })
      .then((res) => {
        //enclosing questions with {} worked. It read the array of properties/questionText

        console.log(res.data);
        alert("Survey Question Form sent!");

        // Clear the questions array after successful save
        // setQuestions([{ questionText: "" }]);
      });
  }

  function questionsUI() {
    return (
      <div className="questionsUIdiv">
        {questions.map((ques, i) => (
          <div key={i} className="questionBox">
            <TextField
              id="standard-multiline-static"
              label="Question"
              multiline
              rows={2}
              style={{ width: "85%" }}
              value={ques.questionText}
              onChange={(e) => {
                changeQuestion(e.target.value, i);
              }}
              variant="filled"
            />

            {/* Each child in a list should have a unique "key" prop. */}

            <FormControl style={{ width: "90%" }}>
              <RadioGroup>
                {ques.options &&
                  ques.options.map((opt, j) => (
                    <div className="addOption_body" key={j}>
                      <FormControlLabel
                        value={ques.options[j].optionText}
                        control={<Radio />}
                        onChange={(e) => {
                          handleRadioChange(e.target.value, i, j);
                        }}
                        label={
                          <TextField
                            style={{ width: "80%" }}
                            className="option_input"
                            placeholder="Option"
                            value={ques.options[j].optionText}
                            onChange={(e) => {
                              changeOptionValue(e.target.value, i, j);
                            }}
                            variant="standard"
                          />
                        }
                      />

                      <div>
                        <CloseIcon
                          onClick={() => {
                            removeOption(i, j);
                          }}
                        ></CloseIcon>
                      </div>
                    </div>
                  ))}
              </RadioGroup>
            </FormControl>

            <div className="addOptionDiv">
              <div className="addOptionChild">
                <AddCircleOutlineIcon
                  onClick={() => {
                    addOption(i);
                  }}
                />
              </div>
              <div className="addOptionChild">
                <p>Add Option</p>
              </div>
            </div>

            {questions.length > 1 && (
              <div className="removeQuestionDiv">
                <div className="removeQuestionChild">
                  <DeleteIcon
                    className="removeQuestionButton"
                    onClick={() => removeQuestionField(i)}
                  />
                </div>
                <div className="removeQuestionChild">
                  <p>Remove Question</p>
                </div>
              </div>
            )}
          </div>
        ))}
      </div>
    );
  }

  return (
    <div className="question_form">
      <Grid container justifyContent="center" alignItems="center">
        <Grid item xs={12} md={7}>
          <div className="form_top">
            <div className="form_title">
              <TextField
                required
                fullWidth
                id="standard-required"
                inputRef={docTitleRef}
                placeholder="Untitled Form"
                variant="standard"
                InputProps={{
                  style: {
                    fontSize: 40,
                  },
                }}
              />
            </div>

            <div className="form_desc">
              <TextField
                fullWidth
                id="standard-required"
                inputRef={docDescRef}
                placeholder="Form Description"
                variant="standard"
              />
            </div>
          </div>
        </Grid>
        <Grid item xs={12} md={7}>
          <div className="questionsUI">{questionsUI()}</div>
        </Grid>
        <Grid item xs={12} md={7}>
          <div className="bottomContainer">
            <div className="addQuestionFieldDiv">
              <div className="addQuestionChild">
                <PlaylistAddIcon
                  className="addQuestionBtn"
                  onClick={addQuestionField}
                />
              </div>
              <div className="addQuestionChild">
                <p>Add New Question</p>
              </div>
            </div>

            <div className="save_form">
              <button className="saveButton" onClick={commitToDB}>
                Save
              </button>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}

export default QuestionForm;
